var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view company_view as
 select s.*, a.street, a.zip_code from company s
 join address a on a.address_id = s.address_id;

 */

exports.getAll = function(callback) {
    var query = 'SELECT * FROM account;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(account_id, callback) {
    var query = 'SELECT a.*, s.skill_name, s.description FROM account a ' +
        'LEFT JOIN account_skill acs on acs.account_id = a.account_id ' +
        'LEFT JOIN skill s on s.skill_id = acs.skill_id ' +
        'WHERE a.account_id = ?';
    var queryData = [account_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE ACCOUNT
    var query = 'INSERT INTO account (email, first_name, last_name) VALUES (?)';

    var queryData = [params.email, params.first_name, params.last_name];

    // NOTE THE EXTRA [] AROUND queryData
    connection.query(query, [queryData], function(err, result) {
        /* OLD
        callback(err, result);
        */
        // THEN USE THE COMPANY_ID RETURNED AS insertId AND THE SELECTED ADDRESS_IDs INTO COMPANY_ADDRESS
        var account_id = result.insertId;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'INSERT INTO account_skill (account_id, skill_id) VALUES ?';

        // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
        var accountSkillData = [];
        for(var i=0; i < params.skill_id.length; i++) {
            accountSkillData.push([account_id, params.skill_id[i]]);
        }

        // NOTE THE EXTRA [] AROUND companyAddressData
        connection.query(query, [accountSkillData], function(err, result){
            callback(err, result);
        });
    });


};

exports.delete = function(account_id, callback) {
    var query = 'DELETE FROM account WHERE account_id = ?';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

//declare the function so it can be used locally
var accountSkillInsert = function(account_id, skillIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO account_skill (account_id, skill_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var accountSkillData = [];
    for(var i=0; i < skillIdArray.length; i++) {
        accountSkillData.push([account_id, skillIdArray[i]]);
    }
    connection.query(query, [accountSkillData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.accountSkillInsert = accountSkillInsert;

//declare the function so it can be used locally
var accountSkillDeleteAll = function(account_id, callback){
    var query = 'DELETE FROM account_skill WHERE account_id = ?';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.accountSkillDeleteAll = accountSkillDeleteAll;

exports.update = function(params, callback) {

    var query = 'UPDATE account SET email = ? WHERE account_id = ?';
    var queryData = [params.email, params.account_id];

    connection.query(query, queryData, function (err, result) {
        var query = 'UPDATE account SET first_name = ? WHERE account_id = ?';
        var queryData = [params.first_name, params.account_id];

        connection.query(query, queryData, function (err, result) {
            var query = 'UPDATE account SET last_name = ? WHERE account_id = ?';
            var queryData = [params.last_name, params.account_id];

            connection.query(query, queryData, function (err, result) {
                accountSkillDeleteAll(params.account_id, function(err, result){

                    if(params.skill_id != null) {
                        //insert company_address ids
                        accountSkillInsert(params.account_id, params.skill_id, function(err, result){
                            callback(err, result);
                        });
                    }
                    else {
                        callback(err, result);
                    }
                });
            });
        });
    });


};

/*  Stored procedure used in this example
     DROP PROCEDURE IF EXISTS company_getinfo;

     DELIMITER //
     CREATE PROCEDURE company_getinfo (company_id int)
     BEGIN

     SELECT * FROM company WHERE company_id = _company_id;

     SELECT a.*, s.company_id FROM address a
     LEFT JOIN company_address s on s.address_id = a.address_id AND company_id = _company_id;

     END //
     DELIMITER ;

     # Call the Stored Procedure
     CALL company_getinfo (4);

 */

exports.edit = function(account_id, callback) {
    var query = 'CALL account_getinfo(?)';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};