var express = require('express');
var router = express.Router();
var account_dal = require('../model/account_dal');
var skill_dal = require('../model/skill_dal');
//var address_dal = require('../model/address_dal');


// View All accounts
router.get('/all', function(req, res) {
    account_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('account/accountViewAll', { 'result':result });
        }
    });

});

// View the account for the given id
router.get('/', function(req, res){
    if(req.query.account_id == null) {
        res.send('account_id is null');
    }
    else {
        account_dal.getById(req.query.account_id, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('account/accountViewById', {'result': result});
           }
        });
    }
});

// Return the add a new company form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    account_dal.getAll(function(err,result) {

        skill_dal.getAll(function(err, result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('account/accountAdd', {'skill': result});
            }
        });

    });
});

// View the account for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.email == null) {
        res.send('email must be provided.');
    }
    else if( (req.query.first_name == null) || (req.query.first_name == null) ) {
        res.send('Name fields must be filled out.');
    }
    else if(req.query.skill_id == null) {
        res.send('At least one skill must be selected');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        account_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/account/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.account_id == null) {
        res.send('An account id is required');
    }
    else {
        account_dal.edit(req.query.account_id, function(err, result){
            res.render('account/accountUpdate', {account: result[0][0], accSkill: result[1], skill: result[2]});
        });
    }

});

router.get('/edit2', function(req, res){
   if(req.query.company_id == null) {
       res.send('An account id is required');
   }
   /*else {
       account_dal.getById(req.query.account_id, function(err, account){
           address_dal.getAll(function(err, address) {
               res.render('account/accountUpdate', {account: account[0] });
           });
       });
   } */

});

router.get('/update', function(req, res) {
    account_dal.update(req.query, function(err, result){
       res.redirect(302, '/account/all');
    });
});

// Delete an account for the given account_id
router.get('/delete', function(req, res){
    if(req.query.account_id == null) {
        res.send('account_id is null');
    }
    else {
         account_dal.delete(req.query.account_id, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 //poor practice, but we will handle it differently once we start using Ajax
                 res.redirect(302, '/account/all');
             }
         });
    }
});

module.exports = router;
